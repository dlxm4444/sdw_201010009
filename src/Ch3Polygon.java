package com.ex.ood.ch3;

import java.awt.*;
import javax.swing.*;
import java.util.*;

//============================================================= Ch3Polygon
abstract class Ch3Polygon {
    private Point center;
    //--------------------------------------------------------- constructor
    public Ch3Polygon(Point center) {
        this.center = center;
    }//end constructor
    
    //--------------------------------------------------------- getCenter
    public Point getCenter() {
        return center;
    }//end getPoint

    //--------------------------------------------------------- draw
    public abstract void draw(Graphics g);
}//endclass Polygon

