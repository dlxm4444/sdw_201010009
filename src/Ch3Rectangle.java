package com.ex.ood.ch3;

import java.awt.*;
import javax.swing.*;
import java.util.*;

class Ch3Rectangle extends Ch3Polygon {
    //--------------------------------------------------------- constructor
    public Ch3Rectangle(Point center) {
        super(center);
    }//end constructor        
    //--------------------------------------------------------- draw
    public void draw(Graphics g) {
        g.drawRect(getCenter().x-20, getCenter().y-10, 40, 20);
    }//end draw
}//endclass Ch3Rectangle

