package com.ex.ood.ch3;

//========================================================= Ch3Comparator3
class Ch3Comparator3 {
    //------------------------------------------------- compare
        public int compare(Object o1, Object o2) {
                if (o1 instanceof String && o2 instanceof String)
                        return ((String) o1).compareTo((String) o2);
                else if (o1 instanceof Integer && o2 instanceof Integer)
                        return ((Integer)o1 - (Integer)o2);
                else
                        return 1;
        }//end compare
}//endclass Ch3Comparator3

