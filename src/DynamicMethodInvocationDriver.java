public class DynamicMethodInvocationDriver
{
	public static void main(String[] args)
	{
		Automobile[] fleet = new Automobile[3];
		fleet[0] = new Sedan(5);
		fleet[1] = new Minivan(6);
		fleet[2] = new SportsCar(2);

		int totalCapacity = 0;
		//version 1
		/*
		for(int i=0; i<fleet.length; i++)
			if(fleet[i] instanceof Sedan)
				totalCapacity += ((Sedan)fleet[i]).getCapacity();
			else if(fleet[i] instanceof Minivan)
				totalCapacity += ((Minivan)fleet[i]).getCapacity();
			else if(fleet[i] instanceof SportsCar)
				totalCapacity += ((SportsCar)fleet[i]).getCapacity();
			else
				totalCapacity += fleet[i].getCapacity();
		*/

		//version 2
		for(int i=0; i<fleet.length; i++)
			totalCapacity += fleet[i].getCapacity();
	}
}
