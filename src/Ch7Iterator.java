package com.ex.ood.ch7;

public interface Ch7Iterator {
    public abstract boolean hasNext();
    public abstract Object next();
}

