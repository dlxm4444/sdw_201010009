package com.ex.ood.ch5;

import java.awt.Point;

class Ch5Tester
{
	public static void main(String[] args)
	{
		//Ch5_2_main();
		Ch5_3_main();
	}

	private static void Ch5_1_main()
	{
		System.out.println("Ch5_1 String immutable");
	
		String str = "sm";
		System.out.println("str = " + str);
		System.out.println("str hashcode = " + System.identityHashCode(str));

		str = str.concat("univ");
		System.out.println("str(concat) = " + str);
		System.out.println("str(concat) hashcode = " + System.identityHashCode(str));
	
		str = str.replace("sm", "000");
		System.out.println("str(replace) = " + str);
		System.out.println("str(replace) hashcode = " + System.identityHashCode(str));

		StringBuffer strBuf = new StringBuffer("sm");
		System.out.println("strBuf = " + strBuf);
		System.out.println("strBuf hashcode = " + System.identityHashCode(strBuf));

		strBuf = strBuf.append("univ");
		System.out.println("strBuf(append) = " + strBuf);
		System.out.println("strBuf(append) hashcode = " + System.identityHashCode(strBuf));
	}

	private static void Ch5_2_main()
	{
		System.out.println("Ch5_2 immutable FixedPoint ===============");
		System.out.println("1) new FixedPoint - no way to change vars");
		Ch5FixedPointV1 fpv1=new Ch5FixedPointV1(3,4);
		System.out.printf("Expected 4 == %2.1f\n", fpv1.getY());
		System.out.printf("Expected 3 == %2.1f\n", fpv1.getX());
		
		System.out.println("2) inheritance - not immutable ------------------------------");
                Ch5FixedPointv2Inheritance fpv2=new Ch5FixedPointv2Inheritance(3,4);
                fpv2.setLocation(5,6);
                System.out.printf("Expected 5 == %2.1f\n", fpv2.getX());
                fpv2.x=7;
                System.out.printf("Expected 7 == %2.1f\n", fpv2.getX());
	}
	
	private static void Ch5_3_main()
	{
		Ch5FixedPointV3Delegation fp = new Ch5FixedPointV3Delegation(3, 4);
		System.out.println("Expected 3 == " + fp.getX()); //prints 3
		Point loc = fp.getLocation();
		loc.x = 5;
		System.out.println("Expected 5 == " + fp.getX()); //prints 5
	}
}
