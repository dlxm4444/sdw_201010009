package com.ex.ood.ch3;

import java.awt.*;
import javax.swing.*;

class Ch3Tester {
    public static void main(String[] args) {
	//Ch3_1_main();	//Polygon GUI
	Ch3_2_main();	//sorter
    }

    private static void Ch3_1_main() {
        System.out.println("---ch3_1 Polygon");

        JFrame frame = new JFrame("TEST");
        frame.setLocation(500, 500);
        frame.setPreferredSize(new Dimension(400, 300));
        Container contentPane = frame.getContentPane();
        
        //Ch3Polygon_v1 drawingPanel = new Ch3Polygon_v1(); // v1, without package
	//Ch3Polygon_v2 drawingPanel = new Ch3Polygon_v2(); // v2
        Ch3Polygon_v3 drawingPanel = new Ch3Polygon_v3(); // v3
        
        contentPane.add(drawingPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private static void Ch3_2_main() {
        System.out.println("Ch3_2 Sorter =====================");

        Integer[] numbers = {new Integer(19), new Integer(79), 
                     new Integer(2), new Integer(8)};
        String[] strings = {"Jin", "Adam", "Digital", "Sangmyung"};
        
        System.out.println("before sorting -------------------");
        for (int i = 0; i < numbers.length; i++) 
            System.out.println("numbers["+i+"]="+numbers[i]);
        for (int i = 0; i < strings.length; i++)
            System.out.println("strings["+i+"]="+strings[i]);
/*
        ///---v1
        System.out.println("V1 -------------------------------");
        Ch3Sorter1.sort(numbers);
        Ch3Sorter1.sort(strings);
        for (int i = 0; i < numbers.length; i++)
            System.out.println("numbers["+i+"]="+numbers[i]);
        for (int i = 0; i < strings.length; i++)
            System.out.println("strings["+i+"]="+strings[i]);

        System.out.println("==================================");
*/
	//---v3
        System.out.println("V3 -------------------------------");
        Ch3Comparator3 comp = new Ch3Comparator3();
        Ch3Sorter3.sort(numbers, comp);
        Ch3Sorter3.sort(strings, comp);
        for (int i = 0; i < numbers.length; i++)
            System.out.println("numbers["+i+"]="+numbers[i]);
        for (int i = 0; i < strings.length; i++)
            System.out.println("strings["+i+"]="+strings[i]);

    }
}

