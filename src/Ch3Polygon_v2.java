package com.ex.ood.ch3;

import java.awt.*;
import javax.swing.*;
import java.util.*;

class Ch3Polygon_v2 extends JPanel {
    //----------------------------------------------------- paint
    public void paint(Graphics g) {
        ArrayList<Ch3PolygonData2> polygons = 
                                        new ArrayList<Ch3PolygonData2>();

        polygons.add(new Ch3PolygonData2("square", new Point(30,30)));
        polygons.add(new Ch3PolygonData2("triangle", new Point(150, 150)));
        polygons.add(new Ch3PolygonData2("rectangle", new Point(200,100)));

        for (int i = 0; i < polygons.size(); i++) {
            String currentPolygon = polygons.get(i).getName();
            Point currentCenter = polygons.get(i).getCenter();
            if (currentPolygon.equals("square"))
                g.drawRect(currentCenter.x -10, 
                           currentCenter.y -10, 20, 20);
            else if (currentPolygon.equals("triangle")) {
                g.drawLine(currentCenter.x, currentCenter.y-10,
                           currentCenter.x-10, currentCenter.y+10);
                g.drawLine(currentCenter.x-10, currentCenter.y+10, 
                           currentCenter.x+10, currentCenter.y+10);
                g.drawLine(currentCenter.x+10, currentCenter.y+10,
                           currentCenter.x, currentCenter.y-10);
            }   
            else if (currentPolygon.equals("rectangle"))
                g.drawRect(currentCenter.x -20, currentCenter.y-10,40,20);
        }
    }
}

