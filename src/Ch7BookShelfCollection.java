package com.ex.ood.ch7;
import java.util.ArrayList;
public class Ch7BookShelfCollection implements Ch7Collection {
    private ArrayList<Ch7Book> books;
        
    public Ch7BookShelfCollection(int maxsize) {
        this.books = new ArrayList<Ch7Book>(maxsize);
    }
    public Ch7Book getBookAt(int index) {
        return books.get(index);
    }
    public void appendBook(Ch7Book book) {
        this.books.add(book);
    }
    public int getLength() {
        return books.size();
    }
    public Ch7Iterator iterator() {
        return new Ch7BookShelfIterator(this);
    }
}

