package com.ex.ood.ch3;

import java.awt.*;
import javax.swing.*;
import java.util.*;

class Ch3Triangle extends Ch3Polygon {
    //--------------------------------------------------------- constructor
    public Ch3Triangle(Point center) {
        super(center);
    }//end constructor
    //--------------------------------------------------------- draw
    public void draw(Graphics g) {
        g.drawLine(getCenter().x, getCenter().y-10, 
                   getCenter().x-10, getCenter().y+10);
        g.drawLine(getCenter().x-10, getCenter().y+10,
                   getCenter().x +10, getCenter().y+10);
        g.drawLine(getCenter().x +10, getCenter().y+10,
                   getCenter().x, getCenter().y-10);
    }//end draw
}//endclass Ch3Triangle

