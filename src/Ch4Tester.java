//Ch4Tester.java
//=====================================================================
package com.ex.ood.ch4;

import java.awt.Color;
import java.awt.Point;

class Ch4Tester {
    public static void main(String[] args) {
       	//Ch4_1_main();    // equals vs. '==' 
	//Ch4_2_main();    // not overriding equals() in Ch4Triangle
        //Ch4_3_main();    // v1 equals() in Ch4Triangle
	//Ch4_4_main();    // not overriding equals() in Ch4ColoredTriangle
	//Ch4_5_main();    // v1 equals() in Ch4ColoredTriangle
        Ch4_6_main();    // v2 equals() in Ch4ColoredTriangle
        //Ch4_7_main();    // v2 equals() in Ch4Triangle & v3 equals() in Ch4ColoredTriangle
        //Ch4_8_main();    // LSP fail
    }

    private static void Ch4_1_main() {
        System.out.println("Ch4_1 equals vs. '==' ============");

        String str1 = "SD2013"; 
        String str2 = "SD2013";

        System.out.println("'str1' hashcode: "+System.identityHashCode(str1));
        System.out.println("'str2' hashcode: "+System.identityHashCode(str2)); 
        System.out.println("'str1 == str2': "+(str1 == str2)); // true (spring pool)
        System.out.println("'str1.equals(str2)': " + str1.equals(str2)); // true

        String str3 = new String("SD2013");
        String str4 = new String("SD2013");
        System.out.println("'str3' hashcode: "+System.identityHashCode(str3)); 
        System.out.println("'str4' hashcode: "+System.identityHashCode(str4)); 
        System.out.println("'str3 == str4': "+(str3 == str4)); // false
        System.out.println("'str3.equals(str4)': " +str3.equals(str4)); // true

        
        int i1 = 1;
        int i2 = 1;
        System.out.println("'i1' hashcode: "+System.identityHashCode(i1));
        System.out.println("'i2' hashcode: "+System.identityHashCode(i2));
        System.out.println("'i1 == i2': "+(i1 == i2)); // true
        //System.out.println("'i1.equals(i2): '" + i1.equals(i2)); 

        Integer i3 = new Integer(1);
        Integer i4 = new Integer(1);
        System.out.println("'Integer i3' hashcode: "+System.identityHashCode(i4));            System.out.println("'Integer i4' hashcode: "+System.identityHashCode(i4));             System.out.println("'i3 == i4': "+(i3 == i4)); 
        System.out.println("'i3.equals(i4)': " + i3.equals(i4)); 
        
        System.out.println("==================================");
    }

    private static void Ch4_3_main() {
        System.out.println("Ch4_3 overriding equals() ========");
        Ch4Triangle triangle1 = new Ch4Triangle(new Point(0,0), new Point(10,10), 
                            new Point(20,20));
        Ch4Triangle triangle2 = new Ch4Triangle(new Point(0,0), new Point(10,10), 
                            new Point(20,20));
        System.out.println(triangle1.equals(triangle2));    

        System.out.println("==================================");
    }

    private static void Ch4_2_main() {
        System.out.println("Ch4_2 not overriding equals() ====");

        Ch4Triangle triangle1 = new Ch4Triangle(new Point(0,0), new Point(10,10), 
                            new Point(20,20));
        Ch4Triangle triangle2 = new Ch4Triangle(new Point(0,0), new Point(10,10), 
                            new Point(20,20));
        System.out.println(triangle1.equals(triangle2));

        System.out.println("==================================");
    }

    private static void Ch4_4_main() {    // p. 101
        System.out.println("Ch4_4 not overriding equals() in Ch4ColoredTriangle ==");
        Ch4Triangle t = new Ch4Triangle(new Point(0,0), new Point(10,10), 
                        new Point(20,20));
        Ch4ColoredTriangle rct = new Ch4ColoredTriangle(Color.red, new Point(0,0), 
                        new Point(10,10), new Point(20,20));
        /*
         symmetric hold
         */        
        System.out.println(t.equals(rct));    // true
        System.out.println(rct.equals(t));    // true    

        System.out.println("-------------------------------------------------------------");

        Ch4ColoredTriangle bct = new Ch4ColoredTriangle(Color.blue, new Point(0,0), 
                        new Point(10,10), new Point(20,20));
        /*
         symmetric, transitive, consistent hold
         */
        System.out.println(rct.equals(bct));    // true 
        System.out.println(bct.equals(rct));    // true
        System.out.println(t.equals(bct));    // true
        System.out.println("===============================");
    }

    private static void Ch4_8_main() {
        System.out.println("Ch4_8 LSP fail =============================");
        Ch4Triangle t1 = new Ch4Triangle(new Point(0,0), new Point(1,1), 
                        new Point(2,2));
        Ch4Triangle t2 = new Ch4ColoredTriangle(Color.red, new Point(0,0), 
                        new Point(1,1), new Point(2,2));
        Ch4Triangle t3 = new Ch4Triangle(new Point(0,0), new Point(1,1),
                                                         new Point(2,2));

        System.out.println("t1.equals(t3): "+t1.equals(t3));
        System.out.println("t2.equals(t3): "+t2.equals(t3));
    }

    private static void Ch4_7_main() {    
        System.out.println("Ch4_7 v2 equals() in Ch4Triangle ==================");
        System.out.println("and   v3 equals() in Ch4ColoredTriangle =========");
        Ch4Triangle t = new Ch4Triangle(new Point(0,0), new Point(10,10), 
                        new Point(20,20));
        Ch4ColoredTriangle rct = new Ch4ColoredTriangle(Color.red, new Point(0,0),
                            new Point(10,10), new Point(20,20));
        Ch4ColoredTriangle bct = new Ch4ColoredTriangle(Color.blue, new Point(0,0), 
                            new Point(10,10), new Point(20,20));
        /*
         symmetric, transitive, consistent hold
         */    
        System.out.println(rct.equals(t));    // false
        System.out.println(t.equals(bct));    // false    
        System.out.println(rct.equals(bct));    // false
    }

    private static void Ch4_6_main() {    
        System.out.println("Ch4_6 v2 equals() in Ch4ColoredTriangle ========");
        Ch4Triangle t = new Ch4Triangle(new Point(0,0), new Point(10,10), 
                        new Point(20,20));
        Ch4ColoredTriangle rct = new Ch4ColoredTriangle(Color.red, new Point(0,0), 
                        new Point(10,10), new Point(20,20));
        Ch4ColoredTriangle bct = new Ch4ColoredTriangle(Color.blue, new Point(0,0), 
                        new Point(10,10), new Point(20,20));
        /*
         transitive fail
         */    
        System.out.println(rct.equals(t));    // true
        System.out.println(t.equals(bct));    // true    
        System.out.println(rct.equals(bct));    // false
        
    }

    private static void Ch4_5_main() {    // p. 102
        System.out.println("Ch4_5 v1 equals() in Ch4ColoredTriangle =========");
        Ch4Triangle t = new Ch4Triangle(new Point(0,0), new Point(10,10), 
                        new Point(20,20));
        Ch4ColoredTriangle rct = new Ch4ColoredTriangle(Color.red, new Point(0,0), 
                        new Point(10,10), new Point(20,20));
        /*
         symmetric fail
         */        
        System.out.println(t.equals(rct));    // true
        System.out.println(rct.equals(t));    // false    
    }

}

