class SportsCar extends Automobile
{
	int capacity;

	public SportsCar(int capacity)
	{
		this.capacity = capacity;
	}

	public int getCapacity()
	{
		return capacity;
	}
}
