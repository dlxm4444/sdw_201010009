package com.ex.ood.ch7;

class Ch7PrintBannerDelegation implements Ch7Print
{
	private Ch7Banner banner;

	public Ch7PrintBannerDelegation(String string) {
        	this.banner = new Ch7Banner(string);
	}
	public void printStrong() {
		banner.showWithAster();
	}
	public void printWeak() {
		banner.showWithParen();
	}
}
