import java.util.List;
import java.util.LinkedList;

public class ListExample
{
	public static void main(String[] args)
	{
		List<String> cities = new LinkedList<String>();

		cities.add("Seoul");
		cities.add("Suwon");
		cities.add("Busan");
		
		System.out.println("Ch2_5 LinkedList =========");
		
		for(int x=cities.size()-1; x>=0; x--)
			System.out.println("city " + x + " is " + cities.get(x));

		System.out.println("add ---------");

		cities.add(0, "Daejon");
	
		for(int x=cities.size()-1; x>=0; x--)
			System.out.println("city " + x + " is " + cities.get(x));
	
		System.out.println("==========================");
	}
}
