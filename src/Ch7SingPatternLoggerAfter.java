package com.ex.ood.ch7;

public class Ch7SingPatternLoggerAfter {
    private static Ch7SingPatternLoggerAfter instance = null;
    private Ch7SingPatternLoggerAfter() {
	System.out.println("only one instance");
    }

    public static synchronized Ch7SingPatternLoggerAfter instance() {
    	if(instance == null)
		instance = new Ch7SingPatternLoggerAfter();

	return instance;
    }

    public void readEntireLog() {
        System.out.println("Normal log goes here");
    }
}

