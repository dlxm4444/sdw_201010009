package com.ex.ood.ch7;

public class Ch7PrintBannerInheritance extends Ch7Banner implements Ch7Print {
    public Ch7PrintBannerInheritance(String string) {
        super(string);
    }
    public void printStrong() {
        showWithAster();
    }
    public void printWeak() {
        showWithParen();
    }
}

