public class OverrideDriver
{
	public static void main(String[] args)
	{
		Object1 obj = new Object1();
		Automobile3 auto = new Automobile3();
		Object1 autoObj = new Automobile3();

		auto.equals(obj);
		auto.equals(auto);
		auto.equals(autoObj);
		System.out.println("----------");
	
		obj.equals(obj);
		obj.equals(auto);
		obj.equals(autoObj);
		System.out.println("----------");

		autoObj.equals(obj);
		autoObj.equals(auto);
		autoObj.equals(autoObj);
	}
}
