package com.ex.ood.ch7;
import java.util.*;

public class Ch7IDCardFactory extends Ch7Factory {
    private Vector<String> owners = new Vector<String>();
    protected Ch7Product createProduct(String owner) {
        return new Ch7IDCard(owner);
    }
    protected void registerProduct(Ch7Product product) {
        owners.add(((Ch7IDCard)product).getOwner());
    }
    public Vector getOwners() {
        return owners;
    }
}

