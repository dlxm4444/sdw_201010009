public class OverloadDriver
{
	public static void main(String[] args)
	{
		Object1 obj = new Object1();
		Automobile2 auto = new Automobile2();
		Object1 autoObj = new Automobile2();
		
		//auto의 type은 Automobile2이고
		//Automobile2은 Object1을 상속받으므로
		// overload된 equals mothod 2개를 가지고 있고
		//호출시의 인자에 따라 구분하여
		//가장 적절한(parameter와 대응가능한) method가 호출된다. 
		auto.equals(obj);
		auto.equals(auto);
		auto.equals(autoObj);

		//전부 다 equals(Object1 obj)가 호출된다.
		//equals(Automobile auto) mothod가 없다.
		obj.equals(obj);
		obj.equals(auto);	
		obj.equals(autoObj);

		//이하동문
		autoObj.equals(obj);
		autoObj.equals(auto);
		autoObj.equals(autoObj);
	}
}
