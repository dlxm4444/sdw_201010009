package com.ex.ood.ch3;

class Ch3Sorter1 {
    //------------------------------------------------------- sort
    public static void sort(Integer[] data) {
        for (int i = data.length-1; i >= 1; i--) {
            int indexOfMax = 0;
            for (int j = 1; j <= i; j++) {
                if (data[j] > data[indexOfMax])
                    indexOfMax = j;
            }
            Integer temp = data[i];
            data[i] = data[indexOfMax];
            data[indexOfMax] = temp;
        }
    }//end sort    
    //------------------------------------------------------- sort
    public static void sort(String[] data) {
        for (int i = data.length-1; i >= 1; i--) {
            int indexOfMax = 0;
            for (int j = 1; j <= i; j++) {
                if (data[j].compareTo(data[indexOfMax]) > 0)
                    indexOfMax = j;
            }
            String temp = data[i];
            data[i] = data[indexOfMax];
            data[indexOfMax] = temp;
        }
    }//end sort
}//endclass Ch3Sorter1

