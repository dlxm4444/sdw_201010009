class Sedan extends Automobile
{
	int capacity;
	
	public Sedan(int capacity)
	{
		this.capacity = capacity;
	}

	public int getCapacity()
	{
		return capacity;
	}
}
