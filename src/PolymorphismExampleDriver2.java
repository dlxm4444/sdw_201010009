public class PolymorphismExampleDriver2
{

	public static void main(String[] args)
	{
		Colorable colorableRefVar = new PlainText();
		colorableRefVar = new CompoundText();
		colorableRefVar = new Circle();
		Changeable changeableRefVar = new Circle();
	
		//colorableRefVar = new Changeable();
		//interface는 객체를 만들 수 없음
	}
}
