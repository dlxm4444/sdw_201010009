package com.ex.ood.ch7;

class Ch7Tester{
    public static void main(String[] args) {
	//Ch7_1_main();    //Adapter Pattern
	//Ch7_2_main();    //Singleton Pattern
	//Ch7_3_main();    //Singletoe Pattern2
	//Ch7_4_main();
	//Ch7_5_main();
	Ch7_6_main();
    }

    private static void Ch7_1_main() {
        //Ch7Print p = new Ch7PrintBannerInheritance("Hello");
        Ch7Print p = new Ch7PrintBannerDelegation("Hello");
        p.printStrong();
        p.printWeak();
    }       

    private static void Ch7_2_main() {
        System.out.println("Singleton Pattern");
        Ch7Singleton obj1 = Ch7Singleton.instance();
        Ch7Singleton obj2 = Ch7Singleton.instance();
    
	System.out.println("NO Singleton --------------------------------------");
        Ch7SingPatternLoggerBefore loggerB = new Ch7SingPatternLoggerBefore();
        loggerB.readEntireLog();
    }

    private static void Ch7_3_main()
    {
        System.out.println("Singleton Pattern");
        Ch7SingPatternLoggerAfter loggerA = Ch7SingPatternLoggerAfter.instance();
        Ch7SingPatternLoggerAfter loggerB = Ch7SingPatternLoggerAfter.instance();
	loggerA.readEntireLog();
	loggerB.readEntireLog();
    }

    private static void Ch7_4_main() {
        System.out.println("Ch7_4 Iterator Pattern ======================");

        Ch7BookShelfCollection bookShelfCollection = new Ch7BookShelfCollection(4);
        bookShelfCollection.appendBook(new Ch7Book("설득의 심리학"));
        bookShelfCollection.appendBook(new Ch7Book("계축일기"));
        bookShelfCollection.appendBook(new Ch7Book("잼 한 병의 행복"));
        bookShelfCollection.appendBook(new Ch7Book("나는 오늘도 나를 응원한다"));

        Ch7Iterator it = bookShelfCollection.iterator();

        while (it.hasNext()) {
            Ch7Book book = (Ch7Book) it.next();
            System.out.println(book.getName());
        }

        System.out.println("=======================================");
    }

    private static void Ch7_5_main() {
        System.out.println("Ch7_5 Command Pattern =================");

        Ch7SimpleRemoteControl remote = new Ch7SimpleRemoteControl();

        Ch7Light light = new Ch7Light();
        Ch7LightOnCommand lightOn = new Ch7LightOnCommand(light);
        remote.setCommand(lightOn);
        remote.buttonWasPressed();
        
	Ch7LightOffCommand lightOff = new Ch7LightOffCommand(light);
        remote.setCommand(lightOff);	
        remote.buttonWasPressed();

        Ch7GarageDoor gdoor = new Ch7GarageDoor();
        Ch7GarageDoorOpenCommand garageOpen = new  Ch7GarageDoorOpenCommand(gdoor);
        remote.setCommand(garageOpen);
        remote.buttonWasPressed();
        

        Ch7GarageDoorCloseCommand garageClose = new  Ch7GarageDoorCloseCommand(gdoor);
        remote.setCommand(garageClose);
        remote.buttonWasPressed();
        System.out.println("==================================");
    }

    private static void Ch7_6_main() {
        System.out.println("Ch7_6 Factory Pattern ===================");
        Ch7Factory factory = new Ch7IDCardFactory();
        Ch7Product card1 = factory.create("나소설");
        Ch7Product card2 = factory.create("너소설");
        Ch7Product card3 = factory.create("그소설");

        card1.use();
        card2.use();
        card3.use();
        System.out.println("========================================");
    }
}
