package com.ex.ood.ch8.gumballstate;

public interface State {
	public void insertQuarter();
	public void ejectQuarter();
	public void turnCrank();
	public void dispense();    
	//public String toString();
}

