package com.ex.ood.ch8;

public class Ch8PrototypeTester {
    public static void main(String[] args) {
        Ch8SMUFreshman cc = new Ch8SMUFreshman("상명대학교", "디지털미디어학부", "디지털미디어전공");
        try{
            Ch8SMUFreshman clone1 = (Ch8SMUFreshman) cc.clone();
            Ch8SMUFreshman clone2 = (Ch8SMUFreshman) cc.clone();
    
            clone1.setName("나소공");
            clone1.setID("201411111");
            clone2.setName("나설계");
            clone2.setID("201411112");
            
            System.out.println(clone1.toString());
            System.out.println(clone2.toString());

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}

