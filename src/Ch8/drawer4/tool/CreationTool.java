package com.ex.ood.ch8.drawer4.tool;

import com.ex.ood.ch8.drawer4.DrawingCanvas;
import com.ex.ood.ch8.drawer4.figure.Figure;

import java.awt.event.MouseEvent;

public class CreationTool extends Tool{
    private Figure myFigure;

    public CreationTool(Figure s)    {
        myFigure = s;
    }

    public void mouseClicked(MouseEvent e)    {
        Figure newFigure = (Figure) myFigure.clone();
        newFigure.moveTo(e.getX() - newFigure.getBoundingRect().width / 2,
                e.getY() - newFigure.getBoundingRect().height / 2);
        DrawingCanvas canvas = (DrawingCanvas) e.getSource();
        canvas.addFigure(newFigure);
        canvas.unselectAll();
        canvas.selectFigure(newFigure);
    }
}

