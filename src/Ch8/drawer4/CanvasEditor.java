package com.ex.ood.ch8.drawer4;

import com.ex.ood.ch8.drawer4.tool.Tool;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class CanvasEditor implements MouseListener, MouseMotionListener {
    private Tool currentTool;

    //constructor
    public CanvasEditor(Tool tool)    {
        this.currentTool = tool;
    }

    public void setCurrentTool(Tool newTool)    {
        currentTool = newTool;
    }

    public void mouseClicked(MouseEvent e)    {
        currentTool.mouseClicked(e);
    }

    public void mousePressed(MouseEvent e)    {
        currentTool.mousePressed(e);
    }

    public void mouseReleased(MouseEvent e)    {
        currentTool.mouseReleased(e);
    }

    public void mouseEntered(MouseEvent e)    {
        currentTool.mouseEntered(e);
    }

    public void mouseExited(MouseEvent e)    {
        currentTool.mouseExited(e);
    }

    public void mouseDragged(MouseEvent e)    {
        currentTool.mouseDragged(e);
    }

    public void mouseMoved(MouseEvent e)    {
        currentTool.mouseMoved(e);
    }
}

