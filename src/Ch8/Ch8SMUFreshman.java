package com.ex.ood.ch8;

public class Ch8SMUFreshman implements Cloneable {
    private String universityName;
    private String department;
    private String course;
    private String name;
    private String id;
    
    public Ch8SMUFreshman(String uniName, String depart, String course) {
        this.universityName = uniName;
        this.department = depart;
        this.course = course;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setID(String id) {
        this.id = id;
    }

    public String toString() {
        return universityName+" "+department+" "+course+" "+id+" "+name;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Ch8SMUFreshman cc = (Ch8SMUFreshman)super.clone();
        return cc;
    }
}

