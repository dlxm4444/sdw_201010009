package com.ex.ood.ch8.drawer3;

import com.ex.ood.ch8.drawer3.figure.*;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Ch8CanvasEditor implements MouseListener {
    //instance variables
    private Figure currentFigure;

    //constructor
    public Ch8CanvasEditor(Figure initialFigure) {
        this.currentFigure = initialFigure;
    }

    public void setCurrentFigure(Figure newFigure) {
        currentFigure = newFigure;
    }

    public void mouseClicked(MouseEvent e) {
        Figure newFigure = (Figure) currentFigure.clone();
        newFigure.setCenter(e.getX(), e.getY());
        ((Ch8DrawingCanvas) e.getSource()).addFigure(newFigure);
    }

    public void mousePressed(MouseEvent e) { }
    public void mouseReleased(MouseEvent e) { }
    public void mouseEntered(MouseEvent e) { }
    public void mouseExited(MouseEvent e) { }
}

