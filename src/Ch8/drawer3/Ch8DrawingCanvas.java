package com.ex.ood.ch8.drawer3;

import com.ex.ood.ch8.drawer3.figure.*;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;

public class Ch8DrawingCanvas extends JPanel {
    private Ch8Drawing drawing;

    public Ch8DrawingCanvas() {
        this.drawing = new Ch8Drawing();
        setBackground(Color.white);
        setPreferredSize(new Dimension(400, 300));
        setBorder(BorderFactory.createEtchedBorder());
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Figure figure : drawing) {
            figure.draw(g);
        }
    }

    public void addFigure(Figure newFigure) {
        drawing.addFigure(newFigure);
        repaint();
    }
}

