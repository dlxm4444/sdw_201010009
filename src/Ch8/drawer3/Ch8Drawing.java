package com.ex.ood.ch8.drawer3;

import com.ex.ood.ch8.drawer3.figure.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Ch8Drawing implements Iterable<Figure> {
    private List<Figure> figures;  //collection of Figures

    public Ch8Drawing() {
        figures = new ArrayList<Figure>();
    }

    //precondition:  newFigure is non-null and is not already in
    //this Drawing
    public void addFigure(Figure newFigure) {
        figures.add(newFigure);
    }

    public Iterator<Figure> iterator() {
        return figures.iterator();
    }
}

