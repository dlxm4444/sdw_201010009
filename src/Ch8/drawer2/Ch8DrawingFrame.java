package com.ex.ood.ch8.drawer2;

import com.ex.ood.ch8.drawer2.figure.Ellipse;
import com.ex.ood.ch8.drawer2.figure.Rect;
import com.ex.ood.ch8.drawer2.figure.Square;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ch8DrawingFrame extends JFrame {
    //constructor
    public Ch8DrawingFrame() {
	super("Drawing Application");
	setDefaultCloseOperation(EXIT_ON_CLOSE);

	JComponent drawingCanvas = createDrawingCanvas();
	add(drawingCanvas, BorderLayout.CENTER);

	JToolBar toolbar = createToolbar(drawingCanvas);
	add(toolbar, BorderLayout.NORTH);
    }

    private JComponent createDrawingCanvas() {
        JComponent drawingCanvas=new JPanel();
        drawingCanvas.setPreferredSize(new Dimension(400, 300));
        drawingCanvas.setBackground(Color.white);
        drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
        return drawingCanvas;
    }

    private JToolBar createToolbar(JComponent canvas) {
        JToolBar toolbar=new JToolBar();
        JButton ellipseButton=new JButton("Ellipse");
        toolbar.add(ellipseButton);
        JButton squareButton=new JButton("Square");
        toolbar.add(squareButton);
        JButton rectButton=new JButton("Rect");
        toolbar.add(rectButton);

        //add the listeners to the buttons and canvas
        final Ch8CanvasEditor canvasEditor = 
                new Ch8CanvasEditor(new Ellipse(0, 0, 60, 40));
        // 각 버튼에 actionPerformed 넣음 (drawer1에서는 CanvasEditor에 있음)
        // Observer를 anonymous 처리, CanvasEditor implements ActionListener 제거
        ellipseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                canvasEditor.setCurrentFigure(new Ellipse(0, 0, 60, 40));
            }
        });
        squareButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                canvasEditor.setCurrentFigure(new Square(0, 0, 40));
            }
        });
        rectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                canvasEditor.setCurrentFigure(new Rect(0, 0, 60, 40));
            }
        });
        canvas.addMouseListener(canvasEditor);

        return toolbar;
    }
}

