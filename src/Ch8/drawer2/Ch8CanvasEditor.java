package com.ex.ood.ch8.drawer2;

import com.ex.ood.ch8.drawer2.figure.Figure;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Ch8CanvasEditor implements MouseListener {
    //instance variables
    private Figure currentFigure;

    //constructor
    public Ch8CanvasEditor(Figure figure) {
        this.currentFigure = figure;
    }

    public void setCurrentFigure(Figure newFigure) {
        currentFigure = newFigure;
    }

    public void mouseClicked(MouseEvent e) {
        JPanel canvas = (JPanel) e.getSource();
        currentFigure.setCenter(e.getX(), e.getY());
        currentFigure.draw(canvas.getGraphics());
    }

    public void mousePressed(MouseEvent e) { }
    public void mouseReleased(MouseEvent e) { }
    public void mouseEntered(MouseEvent e) { }
    public void mouseExited(MouseEvent e) { }
}

