package com.ex.ood.ch4;

import java.awt.Color;
import java.awt.Point;

class Ch4Triangle {
    private Point p1, p2, p3;
    public Ch4Triangle(Point p1, Point p2, Point p3) {
        if (p1 == null)
            p1 = new Point(10,0);
        if (p2 == null)
            p2 = new Point(0,10);
        if (p3 == null)
            p3 = new Point(20,10);
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }
    
    // version 1 equals() p. 101
    public boolean equals(Object obj)
    {
	if(!(obj instanceof Ch4Triangle))
		return false;
	Ch4Triangle otherTriangle = (Ch4Triangle)obj;
	return (p1.equals(otherTriangle.p1) &&
		p2.equals(otherTriangle.p2) &&
		p3.equals(otherTriangle.p3));
    } 
		
    // Use. Ch4_3_main();    // v1 equals() in Ch4Triangle
    // Use. Ch4_4_main();    // not overriding equals() in Ch4ColoredTriangle

    // version 2 equals() p. 103
    // Use. Ch4_7_main();    // v2 equals() in Ch4Triangle & v3 equals() in Ch4ColoredTriangle
    // Use. Ch4_8_main();    // LSP fail
}

