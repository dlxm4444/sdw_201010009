package com.ex.ood.ch3;

import java.awt.*;
import javax.swing.*;
import java.util.*;

class Ch3Polygon_v3 extends JPanel {
    //--------------------------------------------------------- paint
    public void paint(Graphics g) {
        ArrayList<Ch3Polygon> polygons = new ArrayList<Ch3Polygon>();

        polygons.add(new Ch3Square(new Point(30,30)));
        polygons.add(new Ch3Triangle(new Point(150, 150)));
        polygons.add(new Ch3Rectangle(new Point(200,100)));

        for (Ch3Polygon poly : polygons) {
            poly.draw(g);
        }
    }//end paint
}//endclass Ch3Polygon_v3

