class Minivan extends Automobile
{
	int capacity;

	public Minivan(int capacity)
	{
		this.capacity = capacity;
	}

	public int getCapacity()
	{
		return capacity;
	}
}
