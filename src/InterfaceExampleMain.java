public class InterfaceExampleMain
{
	public static void main(String[] args)
	{
		SimpleRunner concreteObj = new SimpleRunner();
		System.out.println("subclass object, run(): ");
		concreteObj.run();

		Runnable interfaceObj = new SimpleRunner();
		System.out.println("interface object, run(): ");
		interfaceObj.run();
	}
}
