package com.ex.ood.ch3;

//============================================================= Ch3Sorter3
class Ch3Sorter3 {
    //----------------------------------------------------- sort
    public static void sort(Object[] data, Ch3Comparator3 comp) {
        for (int i = data.length-1; i >= 1; i--) {
            int indexOfMax = 0;
            for (int j = 1; j <= i; j++) {
                if (comp.compare(data[j], data[indexOfMax]) > 0)
                    indexOfMax = j;
            }
            Object temp = data[i];
            data[i] = data[indexOfMax];
            data[indexOfMax] = temp;
        }
    }//end sort
}//endclass Ch3Sorter3

