package com.ex.ood.ch3;

import java.awt.*;
import javax.swing.*;
import java.util.*;

class Ch3Square extends Ch3Polygon {
    //--------------------------------------------------------- constructor
    public Ch3Square(Point center) {
        super(center);
    }//end constructor
    //--------------------------------------------------------- draw
    public void draw(Graphics g) {
        g.drawRect(getCenter().x-10, getCenter().y-10, 20, 20);
    }//end draw
}//endclass Ch3Square

