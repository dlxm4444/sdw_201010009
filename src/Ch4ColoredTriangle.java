package com.ex.ood.ch4;

import java.awt.Color;
import java.awt.Point;

class Ch4ColoredTriangle extends Ch4Triangle {
    private Color color;
    public Ch4ColoredTriangle(Color c, Point p1, Point p2, Point p3) {
        super(p1, p2, p3);
        if (c == null)
            c = Color.red;
        color = c;
    }
/*
    // version 1 equals() p. 102    
    public boolean equals(Object obj)
    {
	if(!(obj instanceof Ch4ColoredTriangle))
		return false;
	Ch4ColoredTriangle otherColoredTriangle = (Ch4ColoredTriangle)obj;
	return super.equals(otherColoredTriangle) &&
		this.color.equals(otherColoredTriangle.color);
    }
    // Use. Ch4_5_main();    // overriding equals() in Ch4ColoredTriangle
*/  

    // version 2 equals() p. 103
    // Use. Ch4_6_main();    // v2 equals() in Ch4ColoredTriangle
    public boolean equals(Object obj)
    {
	if(obj instanceof Ch4ColoredTriangle)
	{
		Ch4ColoredTriangle otherColoredTriangle = (Ch4ColoredTriangle)obj;
		return super.equals(otherColoredTriangle) &&
			this.color.equals(otherColoredTriangle.color);
	}
	else if(obj instanceof Ch4Triangle)
		return super.equals(obj);
	else
		return false;
    }
/*
    // version 3 equals() p. 104
    public boolean equals(Object obj)
    {
	if(obj == null)
		return false;
	if(obj.getClass() != this.getClass())
		return false;
	if(!super.equals(obj))
		return false;
	Ch4ColoredTriangle otherColoredTriangle = (Ch4ColoredTriangle)obj;
	return this.color.equals(otherColoredTriangle.color);
    }
    // Use. Ch4_7_main();    // v2 equals() in Ch4Triangle & v3 equals() in Ch4ColoredTriangle
    // Use. Ch4_8_main();    // LSP fail
*/
}

