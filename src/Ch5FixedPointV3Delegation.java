package com.ex.ood.ch5;

import java.awt.Point;

class Ch5FixedPointV3Delegation
{
	private Point pt;
	
	public Ch5FixedPointV3Delegation(Point p)
	{
		this.pt = new Point(p);
	}
	
	public Ch5FixedPointV3Delegation(int x, int y)
	{
		this.pt = new Point(x, y);
	}
	
	public double getX()
	{
		return pt.getX();
	}

	public double getY()
	{
		return pt.getY();
	}

	public Point getLocation()
	{
		return pt.getLocation();
	}
}
