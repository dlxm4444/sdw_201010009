public class PolymorphismExampleDriver
{
	public static void main(String[] args)
	{
		Account AccountRefVar;
		AccountRefVar = new Account();
		AccountRefVar = new CheckingAccount();
		AccountRefVar = new CreditLineAccount();
		AccountRefVar = new CheckingTrafficCardAccount();

		//CheckingAccount CheckingAccountRefVar = new CreditLineAccount();
		//error: cannot assign not compatible object
		//CheckingAccountRefVar = new Account();
		//error: must be cast
	}
}
