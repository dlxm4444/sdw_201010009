package com.ex.ood.ch3;

import java.awt.*;
import javax.swing.*;
import java.util.*;

//========================================================= Ch3PolygoData1
class Ch3PolygonData2 {
    private String name;
    private Point center;
    //----------------------------------------------------- constructor
    public Ch3PolygonData2(String name, Point center) {
        this.name = name;
        this.center = center;
    }//end constructor
    //----------------------------------------------------- getName
    public String getName() {
        return name;
    }//end getName
    //----------------------------------------------------- getCenter
    public Point getCenter() {
        return center;
    }//end getCenter
}//endclass Ch3PolygonData1

