public class Bungeobbang
{
	private String anggeum;
	private String country;

	public Bungeobbang(String anggeum, String country)
	{
		this.anggeum = anggeum;
		this.country = country;
	}

	public String getAnggeum()
	{
		return anggeum;
	}
	
	public String getCountry()
	{
		return country;
	}
	
	public static void main(String[] args)
	{
		Bungeobbang original = new Bungeobbang("팥", "한국");
		Bungeobbang notOriginal = new Bungeobbang("호박", "중국");

		System.out.print("원조 붕어빵 - 앙금: " + original.getAnggeum() + " ");
		System.out.println("원산지: " + original.getCountry());
		System.out.print("짝퉁 붕어빵 - 앙금: " + notOriginal.getAnggeum() + " ");
		System.out.println("원산지: " + notOriginal.getCountry());
	}
}
