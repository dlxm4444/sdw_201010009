import java.util.ArrayList;

public class ArrayListExample
{
	public static void main(String[] args)
	{
		ArrayList<String> cities = new ArrayList<String>();
		cities.add("Seoul");
		cities.add("Suwon");
		cities.add("Busan");
		
		System.out.println("Ch2_5 ArrayList ===========");
		
		for(int x=cities.size()-1; x>=0; x--)
			System.out.println("city " + x + " is " + cities.get(x));

		System.out.println("add ---------");

		cities.add(0, "Daejon");
	
		for(int x=cities.size()-1; x>=0; x--)
			System.out.println("city " + x + " is " + cities.get(x));
	
		System.out.println("==========================");
	}
}
