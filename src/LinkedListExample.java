import java.util.LinkedList;

public class LinkedListExample
{
	public static void main(String[] args)
	{
		LinkedList<String> cities = new LinkedList<String>();

		cities.add("Seoul");
		cities.add("Suwon");
		cities.add("Busan");
		
		System.out.println("Ch2_5 LinkedList =========");
		
		for(int x=cities.size()-1; x>=0; x--)
			System.out.println("city " + x + " is " + cities.get(x));

		System.out.println("add ---------");

		cities.addFirst("Daejon");
	
		for(int x=cities.size()-1; x>=0; x--)
			System.out.println("city " + x + " is " + cities.get(x));
	
		System.out.println("==========================");
	}
}
